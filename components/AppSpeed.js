import React, {useState} from 'react';
import { Text, View, StyleSheet, } from 'react-native';

export default function AppSpeed(props) {
    return (
        <View style={styles.container}>
        <Text style={styles.SpeedNumber}>{props.speed}</Text><Text style={styles.SpeedUnit}>Nds</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:'100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    SpeedNumber:{
        fontSize:200,
    },
    SpeedUnit:{
        fontSize:30,
    }
});
