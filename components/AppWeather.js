import React from 'react';
import { View, Text, StyleSheet} from 'react-native';



export default function AppWeather(props) {
      return(
        <>
            <View style={styles.title}>
            <Text style={styles.WindDirectionTitle}>Vent</Text>
            </View>
            <View style={styles.description}>
            <Text style={styles.WindDirection}>{ props.WindDirection ??  "unknown"}</Text>
            </View>
        </>
      )
}

const styles=StyleSheet.create({
    title:{
        justifyContent:'center',
        borderBottomWidth:StyleSheet.hairlineWidth,
    },
    WindDirectionTitle:{
        fontSize:30,
        textAlign:'center'
    },
    description:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    WindDirection:{
        fontSize:60,
    }
});