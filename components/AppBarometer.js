import React, {useState, useEffect}  from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Barometer } from 'expo-sensors';

export default function AppBarometer() {
    const [data, setData] = useState({});

    useEffect(() => {
        _toggle();
    }, []);

    useEffect(() => {
        return () => {
        _unsubscribe();
        };
    }, []);

    const _toggle = () => {
        if (this._subscription) {
        _unsubscribe();
        } else {
        _subscribe();
        }
    };

    const _subscribe = () => {
        this._subscription = Barometer.addListener(barometerData => {
        setData(barometerData);
        });
    };

    const _unsubscribe = () => {
        this._subscription && this._subscription.remove();
        this._subscription = null;
    };

    const { pressure = 0 } = data;

    return (

        <>
            <View style={styles.title}>
            <Text style={styles.BarometerTitle}>Baromètre</Text>
            </View>
            <View style={styles.description}>
            <Text style={styles.Barometer}>{(pressure/1000)?? '...' } hPa</Text>
            </View>
        </>
    );
}
const styles=StyleSheet.create({
    title:{
        justifyContent:'center',
        borderBottomWidth:StyleSheet.hairlineWidth,
    },
    BarometerTitle:{
        fontSize:30,
        textAlign:'center'
    },
    description:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    Barometer:{
        fontSize:30,
    }
});