import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect}  from 'react';
import { View, StyleSheet, SafeAreaView, Text, RefreshControl, ScrollView } from 'react-native';
import * as Location from 'expo-location';

import AppSpeed from './components/AppSpeed.js'
import AppWeather from './components/AppWeather.js'
import AppBarometer from './components/AppBarometer.js'

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}



export default function App() {
  let speedDisplay = '...';

  const [location, setLocation] = useState(null);
  const [speed, setSpeed] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [windDirection, setWindDirection] = useState('');
  const [weatherInOneHour, setWeatherInOneHour] = useState('');

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(1500).then(() => setRefreshing(false));
  }, []);

  const Separator = () => (
    <View style={styles.separator} />
  );

  useEffect(() => {
    (async () => {
      let {status} = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
      }
      Location.watchPositionAsync({distanceInterval:0.2}, (data) =>{
        setSpeed(data.coords.speed);
      })
      let location = await Location.getCurrentPositionAsync();
      getWeatherFromApi(location.coords.latitude, location.coords.longitude);

    })();
  }, []);

  if (speed != null)
    speedDisplay = speed > 0 ? (speed * 1.9438).toString().match(/\d+\.\d{1}/)[0] : 0;

  async function getWeatherFromApi (latitude, longitude) {
      try {
        let url = `https://api.stormglass.io/v2/weather/point?lat=${latitude}&lng=${longitude}&params=windDirection`;
        let headers = { headers: {'Authorization': 'e9ac0bc4-cf41-11eb-9f40-0242ac130002-e9ac0c46-cf41-11eb-9f40-0242ac130002'}};
          
        let response = await fetch(url, headers);
        let responseJson = await response.json();

        let windDegree = responseJson.hours[0].windDirection.noaa;


        console.log(windDegree);
          if((337.5 <= windDegree && windDegree <= 360) || (0 <= windDegree && windDegree < 22.5))
            setWindDirection('N');
          else if (22.5 <= windDegree && windDegree < 67.5)
            setWindDirection('NE');
          else if (67.5 <= windDegree && windDegree < 112.5)
            setWindDirection('E');
          else if(112.5 <= windDegree  && windDegree< 157.5)
            setWindDirection('SE');
          else if (157.5 <= windDegree && windDegree< 202.5)
            setWindDirection('S');
          else if (202.5 <= windDegree  && windDegree< 247.5)
            setWindDirection('SO');
          else if (247.5 <= windDegree  && windDegree< 292.5)
            setWindDirection('O');
          else if (292.5 <= windDegree  && windDegree< 337.5)
            setWindDirection('NO');
      }
      catch(error){
          console.log(error);
      }
    }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={{width:'100%'}}
        contentContainerStyle={styles.container}
        refreshControl={
        <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
        />}>

        <View style={styles.TopAppContainer}>
          <View style={styles.TopApp}>
          <View style={styles.AppWeatherContainer}>
              <AppWeather WindDirection={windDirection} WeatherInOneHour={weatherInOneHour}/>
              <Separator/>
              <AppBarometer/>
          </View>

          </View>
          <View style={styles.TopApp}>
            <Text>Compass</Text>
          </View>
        </View>

        <View style={styles.AppSpeed}>
          <AppSpeed speed={speedDisplay}></AppSpeed>
        </View>

      </ScrollView>
        <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column'
  },
  separator: {
    marginVertical: 20,
    borderBottomColor: 'black',
    borderBottomWidth: 2,
  },
  TopAppContainer:{
    flex:1,
    flexDirection:'row',
    height:'40%',
  },
  TopApp:{
    flex : 1,
    width:'50%',
    borderWidth:1,
    margin:10,
    padding:10,
    justifyContent:'center',
    alignItems:'center',
  },
  AppWeatherContainer:{
    flexDirection:'column',
    flex:1,
    width:'100%',
  },
  AppSpeed:{
    flex:1,
    borderWidth:1,
    margin:10,
    padding:10,
    justifyContent:'center',
    alignItems:'center',
  }
});
